create database aeg_db;
create schema aeg_schema;

create table Ofertas (
id serial not null,
deporte varchar(50) not null
id_provedores numeric not null,
nombre_oferta varchar(50) not null,
lugar varchar(100) not null,
decripcion varchar(100) not null,
precio money not null
);


create table Usuario (
id serial not null,
nombre_usuario varchar(50) not null,
apellidos varchar(50) not null,
email varchar(50) not null
);

create table Provedores (
id serial not null,
nombre_prov varchar(50) not null
nif varchar(50) not null
direccion varchar(50) not null
email varchar(50) not null
nombre_cont varchar(50) not null
telefono numeric not null
logo image 

);