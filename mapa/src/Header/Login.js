import React, { useState } from 'react'
import './Login.css'

const Login = ({ setUser }) => {
const [ username, setUsername ] = useState('')
const [ password, setPassword ] = useState('')
const handleSubmit = (e) => {
e.preventDefault()
console.log('Login:', username, password)
setUser({
username,
email: 'demo-email@spamherelots.com',
avatar: 'https://i.imgur.com/VVq6KcT.png',
token: 'fake-token'
})
}

return (

<form className="login-form" onSubmit={handleSubmit}>
  <label>
    Username: 
    <input
      name="username"
      required
      value={username}
      onChange={e => setUsername(e.target.value)}
    />
  </label>
  <label>
    Password:
    <input
      name="password"
      type="password"
      required
      value={password}
      onChange={e => setPassword(e.target.value)}
    />

  </label>
  <button>Log in!</button>
  <div>
    <p>SI NO ERES USUARIO</p>
    <p>¡¡REGISTRATE!!</p>
    <button>registrarse</button>
  </div>
</form>
)
}

export default Login