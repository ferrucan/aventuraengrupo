import React from 'react';
import MenuListComposition from './BotonMenuPortada'
import Carrusel from './Carrusel'
import AutoPlayCarousel from './AutoPlayCarrusel'



const Portada = () => {


   return (
      <div className="Portada">

         <div className="MenuListComposition">
            <MenuListComposition />
         </div>
       
         <div className="Carrusel">
            <Carrusel />
         </div>

         <div>
            
          <AutoPlayCarousel />
         </div>



      </div>
   )
}


export default Portada

