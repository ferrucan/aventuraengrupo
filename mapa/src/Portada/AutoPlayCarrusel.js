import React, { render } from 'react';
import Slider from 'infinite-react-carousel';

import SurfingImg from './Imagenes_AutoPlayCarrusel/Surfing.jpg';
import Rutas_4x4Img from './Imagenes_AutoPlayCarrusel/Rutas_4x4.jpg';
import MBTImg from './Imagenes_AutoPlayCarrusel/MBT.jpg';
import ParapenteImg from './Imagenes_AutoPlayCarrusel/Parapente.jpg';
import QuadImg from './Imagenes_AutoPlayCarrusel/Quad.jpg';
import Rutas_BicicletaImg from './Imagenes_AutoPlayCarrusel/Rutas_Bicicleta.jpg';
import Rutas_CaballoImg from './Imagenes_AutoPlayCarrusel/Rutas_Caballo.jpg';
import Rutas_KayakImg from './Imagenes_AutoPlayCarrusel/Rutas_Kayak.jpg';
import Rutas_MotoImg from './Imagenes_AutoPlayCarrusel/Rutas_Moto.jpg';
import SubmarinismoImg from './Imagenes_AutoPlayCarrusel/Submarinismo.jpg';
import WindsurfImg from './Imagenes_AutoPlayCarrusel/Windsurf.jpg';

import './AutoPlayCarrusel.css'

const CarouselAutoPlay = () => {

    const settings = {
        className: 'AutoPlayCarrusel',
        arrowsScroll: 1,
        arrowsBlock: false,
        autoplay: true,
        autoplayScroll: 1,
        autoplaySpeed: 14000,
        duration: 200,
        slidesToShow: 1,
    };
    

    return (
        <div className='AutoPlayCarrusel' >
            <span></span>
             <Slider {...settings}>

                <div >
                    <div className='styleImagen'  style={{ backgroundImage: `url(${SubmarinismoImg})` }}>
                    <p className= 'textoImagenes'>Descubre los nuevos paraisos del buceo</p>
                    </div>
                </div>

                <div>
                    <div className='styleImagen'  style={{ backgroundImage: `url(${SurfingImg})` }}>
                    <p className= 'textoImagenes'>Subete a las olas mas espectaculares </p>
                    </div>
                </div>

                <div>
                    <div className='styleImagen'  style={{ backgroundImage: `url(${Rutas_4x4Img})` }}>
                    <p className= 'textoImagenes'>Conoce a otros viajeros   </p>
                    </div>
                </div>

                <div>
                    <div className='styleImagen'  style={{ backgroundImage: `url(${MBTImg})` }}>
                    <p className= 'textoImagenes'>Únete a un grupo con tus mismas aficiones   </p>  
                    </div>
                </div>

                <div>
                    <div className='styleImagen'  style={{ backgroundImage: `url(${ParapenteImg})` }}>
                    <p className= 'textoImagenes'>Tenemos las actividades mas exclusivas </p> 
                    </div>
                </div>

                <div>
                    <div className='styleImagen'  style={{ backgroundImage: `url(${QuadImg})` }}>  
                     <p className= 'textoImagenes'>Crea tu propio grupo de viajeros   </p> 
                    </div>
                </div>

                <div>
                    <div className='styleImagen'  style={{ backgroundImage: `url(${Rutas_BicicletaImg})` }}>
                    <p className= 'textoImagenes'>Unete a un grupo y disfruta de la ruta  </p> 
                    </div>
                </div>

                <div>
                    <div className='styleImagen'  style={{ backgroundImage: `url(${Rutas_CaballoImg})` }}>
                    <p className= 'textoImagenes'>Unete a un grupo y disfruta de la naturaleza </p> 
                    </div>
                </div>

                <div>
                    <div className='styleImagen'  style={{ backgroundImage: `url(${Rutas_KayakImg})` }}>
                    <p className= 'textoImagenes'>Existen grupos con tus mismas aficiones </p>
                    </div>
                </div>

                <div>
                    <div className='styleImagen'  style={{ backgroundImage: `url(${Rutas_MotoImg})` }}>
                    <p className= 'textoImagenes'>Encuentra viajeros con tu misma ruta  </p>
                    </div>
                </div>

                <div>
                    <div className='styleImagen'  style={{ backgroundImage: `url(${WindsurfImg})` }}>
                    <p className= 'textoImagenes'>Unete a nuestra comunidad y descubre nuevas actividades </p>
                    </div>
                </div>

            </Slider>
        </div>
    );

}
export default CarouselAutoPlay