import React, { render } from 'react';

import Slider from 'infinite-react-carousel';
import { Link } from 'react-router-dom';
import windsurfImg from './Imagen_Portada/windsurf.jpeg';
import puentingImg from './Imagen_Portada/puenting.jpeg';
import velaImg from './Imagen_Portada/vela.jpeg';
import barranquismoImg from './Imagen_Portada/barranquismo.jpeg';
import paracaidismoImg from './Imagen_Portada/paracaidismo.jpeg';
import motosdeaguaImg from './Imagen_Portada/motosdeagua.jpeg';
import submarinismoImg from './Imagen_Portada/submarinismo.jpeg';
import descensoMTBImg from './Imagen_Portada/descensoMTB.jpeg';
import parapenteImg from './Imagen_Portada/parapente.jpeg';
import rutaCaballoImg from './Imagen_Portada/rutaCaballo.jpeg';
import rutaQuadImg from './Imagen_Portada/rutaQuad.jpeg';
import alpinismoImg from './Imagen_Portada/alpinismo.jpeg';
import rutaTodoterrenoImg from './Imagen_Portada/rutaTodoterreno.jpeg';
import surfImg from './Imagen_Portada/surf.jpeg';
import rutaMountainBikeImg from './Imagen_Portada/rutaMountainBike.jpeg';
import kayakImg from './Imagen_Portada/kayak.jpeg';
import ciclismoImg from './Imagen_Portada/ciclismo.jpeg';
import './Carrusel.css'

const Carrusel = () => {

  const settings = {
    autoplay: true,
    arrowsScroll: 2,
    arrowsBlock: true,
    autoplayScroll: 1,
    centerPadding: 20,
    overScan: 6,
    slidesToShow: 7,
    wheelScroll: 1
  };
  return (
    <div className='Carrusel'>

      <Slider {...settings}>

        <nav>
          <Link path='https://......../vela'>
            <div className='imagenCarrusel' title='REGATAS A VELA'>
              <div style={{ height: 135, width: 160, backgroundImage: `url(${velaImg})` }}>
                <p className='imagenCarruselText'>REGATAS A VELA</p>
              </div>
            </div>
          </Link>
        </nav>

        <nav>
          <Link path='https://......../RutasMountainBike'>
            <div className='imagenCarrusel' title='RUTAS MOUNTAINBIK'>
              <div style={{ height: 135, width: 160, backgroundImage: `url(${rutaMountainBikeImg})` }}>
                <p className='imagenCarruselText'>RUTAS MOUNTAINBIKE</p>
              </div>
            </div>
          </Link>
        </nav>

        <nav>
          <Link path='https://......../rutas_cicloturismo'>
            <div className='imagenCarrusel' title='RUTAS DE CICLOTURISMO'>
              <div style={{ height: 135, width: 160, backgroundImage: `url(${ciclismoImg})` }}>
                <p className='imagenCarruselText'>RUTAS DE CICLOTURISMO</p>
              </div>
            </div>
          </Link>
        </nav>

        <nav>
          <Link path='https://......../kayak'>
            <div className='imagenCarrusel' title='RUTAS EN KAYAK'>
              <div style={{ height: 135, width: 160, backgroundImage: `url(${kayakImg})` }}>
                <p className='imagenCarruselText'>RUTAS EN KAYAK</p>
              </div>
            </div>
          </Link>
        </nav>

        <nav>
          <Link path='https://......../alpinismo'>
            <div className='imagenCarrusel' title='RUTAS DE ALPINISMO'>
              <div style={{ height: 135, width: 160, backgroundImage: `url(${alpinismoImg})` }}>
                <p className='imagenCarruselText'>RUTAS DE ALPINISMO</p>
              </div>
            </div>
          </Link>
        </nav>

        <nav>
          <Link path='https://......../puenting'>
            <div className='imagenCarrusel' title='PUENTING'>
              <div style={{ height: 135, width: 160, backgroundImage: `url(${puentingImg})` }}>
                <p className='imagenCarruselText'>PUENTING</p>
              </div>
            </div>
          </Link>
        </nav>

        <nav>
          <Link path='https://......../rutas_quad'>
            <div className='imagenCarrusel' title='RUTAS EN QUAD' >
              <div style={{ height: 135, width: 160, backgroundImage: `url(${rutaQuadImg})` }}>
                <p className='imagenCarruselText'>RUTAS EN QUAD</p>
              </div>
            </div>
          </Link>
        </nav>

        <nav>
          <Link path='https://......../rutas_caballo'>
            <div className='imagenCarrusel' title='RUTAS A CABALLO' >
              <div style={{ height: 135, width: 160, backgroundImage: `url(${rutaCaballoImg})` }}>
                <p className='imagenCarruselText'>RUTAS A CABALLO</p>
              </div>
            </div>
          </Link>
        </nav>

        <nav>
          <Link path='https://......../parapente'>
            <div className='imagenCarrusel' title='PARAPENTE'>
              <div style={{ height: 135, width: 160, backgroundImage: `url(${parapenteImg})` }}>
                <p className='imagenCarruselText'>PARAPENTE</p>
              </div>
            </div>
          </Link>
        </nav>

        <nav>
          <Link path='https://......../paracaidismo'>
            <div className='imagenCarrusel' title='PARACAIDISMO'>
              <div style={{ height: 135, width: 160, backgroundImage: `url(${paracaidismoImg})` }}>
                <p className='imagenCarruselText'>PARACAIDISMO</p>
              </div>
            </div>
          </Link>
        </nav>

        <nav>
          <Link path='https://......../moto_de_agua'>
            <div className='imagenCarrusel' title='MOTOS DE AGUA'>
              <div style={{ height: 135, width: 160, backgroundImage: `url(${motosdeaguaImg})` }}>
                <p className='imagenCarruselText'>MOTOS DE AGUA</p>
              </div>
            </div>
          </Link>
        </nav>

        <nav>
          <Link path='https://......../submarinismo'>
            <div className='imagenCarrusel' title='SUBMARINISMO' >
              <div style={{ height: 135, width: 160, backgroundImage: `url(${submarinismoImg})` }}>
                <p className='imagenCarruselText'>SUBMARINISMO</p>
              </div>
            </div>
          </Link>
        </nav>

        <nav>
          <Link path='https://......../rutas_descenso_MTB'>
            <div className='imagenCarrusel' title='DESCENSOS MTB'>
              <div style={{ height: 135, width: 160, backgroundImage: `url(${descensoMTBImg})` }}>
                <p className='imagenCarruselText'>DESCENSOS MTB</p>
              </div>
            </div>
          </Link>
        </nav>

        <nav>
          <Link path='https://......../rutas_todo_terreno'>
            <div className='imagenCarrusel' title='RUTAS EN TODOTERRENO'>
              <div style={{ height: 135, width: 160, backgroundImage: `url(${rutaTodoterrenoImg})` }}>
                <p className='imagenCarruselText'>RUTAS EN TODOTERRENO</p>
              </div>
            </div>
          </Link>
        </nav>

        <nav>
          <Link path='https://......../surf'>
            <div className='imagenCarrusel' title='SURFING' >
              <div style={{ height: 135, width: 160, backgroundImage: `url(${surfImg})` }}>
                <p className='imagenCarruselText'>SURFING</p>
              </div>
            </div>
          </Link>
        </nav>

        <nav>
          <Link path='https://......../windsurf'>
            <div className='imagenCarrusel' title='WINDSURF'>
              <div style={{ height: 135, width: 160, backgroundImage: `url(${windsurfImg})` }}>
                <p className='imagenCarruselText'>WINDSURF</p>
              </div>
            </div>
          </Link>
        </nav>

        <nav>
          <Link path='https://......../barranquismo'>
            <div className='imagenCarrusel' title='BARRANQUISMO' >
              <div style={{ height: 135, width: 160, backgroundImage: `url(${barranquismoImg})` }}>
                <p className='imagenCarruselText'>BARRANQUISMO</p>
              </div>
            </div>
          </Link>
        </nav>

      </Slider>
    </div>
  );


}
export default Carrusel