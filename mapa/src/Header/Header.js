import React, {useState}from 'react';
import './Header.css'
import Login from './Login'
import { Link } from 'react-router-dom'

const Header= () => {

const [ pulse, setPulse ] = useState(false)
const Handleclick = () => setPulse(!pulse)
       
return(
<div className='header'>
    <Link to='/'>
    <button className='buttonheader'>
        AVENTURA EN GRUPO
    </button>
    </Link>   
    
    <button className='buttonloggin' onClick={Handleclick}>
    login
    {pulse && <div> <Login/></div>}
    </button>
   
</div>
)
}
export default Header
