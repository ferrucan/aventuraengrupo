import React from 'react'
import Portada from './Portada/Portada'
import Header from './Header/Header'
import './App.css'
import Ofer from './ofer'
import Ofertas from './Ofertas/Ofertas';
import Opciones from './Opciones/Opciones';
import Footer from './Footer/Footer';

import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom"
// import Header from './Header/Header'

function App() {
  return (
    <div className= 'fondoApp'>
    <Router>
      <div >
       <Header/>
        <Switch>
          <Route path='/'exact>
            <body className='body'>
              <Portada />
              <Opciones />
              <Ofertas />
            </body>                
          </Route>
          <Route path='/ofer'>
            <Ofer />
          </Route>
        </Switch>
        <Footer/>
      </div>
    </Router>
    </div>
  );
}

export default App;
