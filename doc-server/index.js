// Import express
let express = require('express');
// Import Body parser
let bodyParser = require('body-parser');

const db = require('./controller')
// Initialise the app
let app = express();



// Configure bodyparser to handle post requests
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.set('trust proxy', true)

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


// Setup server port
var port = process.env.PORT || 3000;


// Send message for default URL
app.get('/test', (req, res) => res.send('Express is running'));

app.get('/users', db.getUsers)
app.post('/users', db.createUser)



app.listen(port, () => {
    console.log(`App running on port ${port}.`)
  })