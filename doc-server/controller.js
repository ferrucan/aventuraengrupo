
const Pool = require('pg').Pool

const pool = new Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'aeg',
  password: '123456',
  port: 5432,
})

const getUsers = (request, response) => {
  pool.query('SELECT * FROM usuario ORDER BY id ASC', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const createUser = (request, response) => {
  const { fullname, pass, email } = request.body

  pool.query('INSERT INTO usuario (fullname, pass, email) VALUES ($1, $2, $3)', [fullname, pass, email], (error, results) => {
    if (error) {
      throw error
    }
    response.status(201).send(`User added with ID: ${results.id}`) 
  })
}

module.exports = {
  getUsers,
  createUser,
}